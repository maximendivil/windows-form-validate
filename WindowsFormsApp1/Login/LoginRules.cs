﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.Services;

namespace WindowsFormsApp1.Login
{
    public class LoginRules
    {
        private readonly Login Form;

        public LoginRules(Login form)
        {
            this.Form = form;
        }

        public Dictionary<string, object> Rules {
            get {
                Dictionary<String, object> UsernameRules = new Dictionary<string, object>
                {
                    { ValidatorOptions.Label, this.Form.labelUsername.Name },
                    { ValidatorOptions.LabelError, this.Form.labelUsernameError.Name },
                    { ValidatorOptions.Required, true },
                    { ValidatorOptions.MinLength, 3 },
                    { ValidatorOptions.MaxLength, 10 },
                    { ValidatorOptions.Touched, false }
                };

                Dictionary<String, object> PasswordRules = new Dictionary<string, object>
                {
                    { ValidatorOptions.Label, this.Form.labelPassword.Name },
                    { ValidatorOptions.LabelError, this.Form.labelPasswordError.Name },
                    { ValidatorOptions.Required, true },
                    { ValidatorOptions.MinLength, 6 },
                    { ValidatorOptions.Touched, false }
                };

                Dictionary<string, object> ValidationRules = new Dictionary<string, object>
                {
                    { this.Form.textBoxUsername.Name, UsernameRules },
                    { this.Form.textBoxPassword.Name, PasswordRules }
                };

                return ValidationRules;
            }
        }
    }
}
