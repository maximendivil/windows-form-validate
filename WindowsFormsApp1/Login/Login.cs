﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Login;
using WindowsFormsApp1.Services;

namespace WindowsFormsApp1.Login
{
    public partial class Login : Form
    {
        private readonly Validator Validator;

        public Login()
        {
            InitializeComponent();
            
            Validator = new Validator
            {                
                Form = this,
                Rules = new LoginRules(this).Rules,
                SubmitButton = this.buttonSubmit
            };
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Validator.MarkAllInputsAsTouched();
            Validator.ValidateInputs();
        }

        private void TextBoxUsername_TextChanged(object sender, EventArgs e)
        {
            Validator.ValidateInputs();
        }

        private void TextBoxPassword_TextChanged(object sender, EventArgs e)
        {
            Validator.ValidateInputs();
        }       

        private void TextBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            Validator.MarkInputAsTouched((Control)sender);
        }

        private void TextBoxUsername_KeyDown(object sender, KeyEventArgs e)
        {
            Validator.MarkInputAsTouched((Control)sender);
        }
    }
}
