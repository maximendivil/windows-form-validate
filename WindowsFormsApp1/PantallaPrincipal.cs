﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.hora.Text = DateTime.Now.ToString();
        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {
            this.comboBox1.Items.Add("Libres");
            this.comboBox1.Items.Add("Ocupados");
            this.comboBox1.Items.Add("Todos");
            this.comboBox1.SelectedIndex = 0;
        }
    }
}
