﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Services
{
    public class Validator
    {
        public Dictionary<string, object> Rules { get; set; }
        public Form Form { get; set; }
        public Control SubmitButton { get; set; }

        public void ValidateInputs()
        {
            List<bool> inputState = new List<bool>();
            bool controlValid = true;
            foreach (KeyValuePair<string, object> entry in Rules)
            {
                Control control = this.Form.Controls.Find(entry.Key, true).FirstOrDefault();
                if (control != null)
                {
                    Dictionary<string, object> rules = (Dictionary<string, object>)entry.Value;
                    Control label = this.Form.Controls.Find(rules[ValidatorOptions.Label].ToString(), true).FirstOrDefault();
                    Control labelError = this.Form.Controls.Find(rules[ValidatorOptions.LabelError].ToString(), true).FirstOrDefault();
                    if (rules.Keys.Contains(ValidatorOptions.Touched) && ((bool)rules[ValidatorOptions.Touched]))
                    {
                        controlValid = true;

                        if (rules.Keys.Contains(ValidatorOptions.Required) && ((bool)rules[ValidatorOptions.Required] && control.Text.Length.Equals(0)))
                        {
                            if (controlValid)
                                AddErrorStyle(control, label, labelError, ValidatorMessages.Required);
                            controlValid = false;
                        }

                        if (rules.Keys.Contains(ValidatorOptions.Number) && ((bool)rules[ValidatorOptions.Number]))
                        {
                            try
                            {
                                Int64.Parse(control.Text);
                            }
                            catch (Exception ex)
                            {
                                if (ex is FormatException || ex is OverflowException)
                                {
                                    if (controlValid)
                                        AddErrorStyle(control, label, labelError, ValidatorMessages.Number);
                                    controlValid = false;
                                    return;
                                }

                                throw;
                            }
                        }

                        if (rules.Keys.Contains(ValidatorOptions.MinLength) && (control.Text.Length < (int)rules[ValidatorOptions.MinLength]))
                        {
                            if (controlValid)
                                AddErrorStyle(control, label, labelError, String.Format(ValidatorMessages.MinLength, (int)rules[ValidatorOptions.MinLength]));
                            controlValid = false;
                        }

                        if (rules.Keys.Contains(ValidatorOptions.MaxLength) && (control.Text.Length > (int)rules[ValidatorOptions.MaxLength]))
                        {
                            if (controlValid)
                                AddErrorStyle(control, label, labelError, String.Format(ValidatorMessages.MaxLength, (int)rules[ValidatorOptions.MaxLength]));
                            controlValid = false;
                        }

                        if (controlValid)
                        {
                            RemoveErrorStyle(control, label, labelError);                            
                        }

                        inputState.Add(controlValid);                        
                    }
                }
            }

            if (inputState.Where(x => !x).Any())
            {
                this.SubmitButton.Enabled = false;
            }
            else
            {
                this.SubmitButton.Enabled = true;
            }
        }

        public void MarkInputAsTouched(Control control)
        {
            Dictionary<string, object> rules = (Dictionary<string, object>)this.Rules[control.Name];
            rules[ValidatorOptions.Touched] = true;
        }

        public void MarkAllInputsAsTouched()
        {
            foreach (KeyValuePair<string, object> entry in this.Rules)
            {
                Control control = this.Form.Controls.Find(entry.Key, true).FirstOrDefault();
                if (control != null)
                {
                    Dictionary<string, object> rules = (Dictionary<string, object>)entry.Value;
                    rules[ValidatorOptions.Touched] = true;
                }
            }
        }

        private void AddErrorStyle(Control control, Control label, Control labelError, String errorText)
        {
            control.BackColor = Color.Salmon;
            if (label != null)
                label.ForeColor = Color.Red;
            if (labelError != null)
            {
                labelError.Text = errorText;
                labelError.Visible = true;
            }
        }

        private void RemoveErrorStyle(Control control, Control label, Control labelError)
        {
            control.BackColor = Color.White;
            if (label != null)
                label.ForeColor = Color.Black;
            if (labelError != null)
            {
                labelError.Text = String.Empty;
                labelError.Visible = false;
            }
        }
    }

    public static class ValidatorOptions
    {
        public static string Label = "Label";
        public static string LabelError = "LabelError";
        public static string MaxLength = "MaxLength";
        public static string MinLength = "MinLength";
        public static string Number = "Number";
        public static string Required = "Required";
        public static string Touched = "Touched";
    }

    public static class ValidatorMessages
    {
        public static string MaxLength = "No puede superar los {0} caracteres";
        public static string MinLength = "Debe ingresar al menos {0} caracteres";
        public static string Number = "Por favor ingrese sólo números";
        public static string Required = "Por favor ingrese un valor";
    }
}
